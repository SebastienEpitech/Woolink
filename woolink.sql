-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Ven 25 Août 2017 à 11:30
-- Version du serveur :  5.7.19-0ubuntu0.16.04.1
-- Version de PHP :  7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `woolink`
--

-- --------------------------------------------------------

--
-- Structure de la table `bureau`
--

CREATE TABLE `bureau` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `milieu` varchar(255) NOT NULL,
  `lieu` varchar(255) NOT NULL,
  `prix` float NOT NULL,
  `lienIMG` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `reserve` tinyint(1) NOT NULL DEFAULT '0',
  `garantie` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `bureau`
--

INSERT INTO `bureau` (`id`, `nom`, `milieu`, `lieu`, `prix`, `lienIMG`, `description`, `reserve`, `garantie`) VALUES
(2, 'bureauA', '16eme', 'suchet', 250, 'img/bureau/bureauA.jpg', '', 0, 0),
(3, 'bureauC', '16eme', 'suchet', 300, 'img/bureau/bureauC.jpg', 'bureau en bois', 0, 400),
(4, 'bureauB', '2eme', 'Grand Boulevard', 541, 'img/bureau/bureauB.jpg', '', 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `compte`
--

CREATE TABLE `compte` (
  `id` int(11) NOT NULL,
  `nomSociete` varchar(255) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `mdp` varchar(255) DEFAULT NULL,
  `siret` varchar(255) NOT NULL,
  `dateDebut` varchar(255) NOT NULL,
  `nomRepresentant` varchar(255) NOT NULL,
  `prenomRepresentant` varchar(255) NOT NULL,
  `tel` varchar(255) NOT NULL,
  `categorie` varchar(255) NOT NULL,
  `description` text,
  `valide` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `compte`
--

INSERT INTO `compte` (`id`, `nomSociete`, `mail`, `mdp`, `siret`, `dateDebut`, `nomRepresentant`, `prenomRepresentant`, `tel`, `categorie`, `description`, `valide`) VALUES
(25, 'lolo', 'momo@baba.ocm', 'zadza', '4552215', '2016-02-02', 'momo', 'baba', '077445889662', 'commerce', 'zmplr', 0),
(26, 'societe A', 'alain.dupond@gmail.com', 'azerty', '52487963', '2017-06-27', 'Dupond', 'Alain', '0660152453', 'informatique', 'Bonjour, nous sommes une société de A.', 0),
(27, 'essai', 'try@gmail.com', 'mmm', '4589625', '2017-06-30', 'Try', 'Bernard', '0774758695', 'informatique', 'non', 0),
(31, 'Arbre 2.0', 'marguerite.vege@gmail.com', 'pouet', '5489652350127891', '2017-07-04', 'Vege', 'Marguerite', '0785964521', 'informatique', 'bonjour, on est une entreprise sur les arbres.', 0),
(36, 'Poney', 'pan.bambi@gmail.com', 'panpan', '456213810652', '2017-07-04', 'bambi', 'pan', '045965745102', 'medical', '', 0),
(37, 'Horses', 'pierre.marchant@gmail.com', 'pierre', '524879621', '2017-07-10', 'Marchant', 'Pierre', '07485695235', 'informatique', 'entreprise des chevaux', 0),
(39, 'zr', 'zed@zef', 'qef', 'fezf', '2017-07-25', 'ezfez', 'qef', 'qef', 'commerce', 'eeeeeeeeef', 0),
(41, 'qzf', 'qzf@qzd', 'qsfqs', 'zf', '2017-07-25', 'qzf', 'qzs', 'qf', 'commerce', 'azf', 0),
(46, 'titi', 'thibaut.vitaly@titi.com', 'pouet', '54212525', '2017-06-19', 'Vitaly', 'Thibaut', '0654895235', 'informatique', 'fdtrdes', 0),
(47, 'Woolink', 'woolink@admin.com', 'woolink1234', 'none', '2017-08-11', 'none', 'none', 'none', 'none', '', 0),
(50, 'MMM', 'moleinsmor@eisti.eu', 'mmm', '8521235', '27/10/2017', 'Moleins', 'Morgane', '0774586952', 'informatique', NULL, 0),
(52, 'qs', 'ojo@kio', NULL, '453', '25/10/2017', 'ookj', 'joo', '5784', 'informatique', NULL, 0),
(60, 'mmm', 'joideie@ff.do', NULL, '552', '4/10/2017', 'kdzk', 'ef', '05050505', 'commerce', NULL, 0),
(65, 'deedde', 'ezaf@jygfn.df', NULL, '5651231', '28/10/2017', 'qd', 'azd', '3375353', 'informatique', NULL, 0);

-- --------------------------------------------------------

--
-- Structure de la table `lienESB`
--

CREATE TABLE `lienESB` (
  `id` int(11) NOT NULL,
  `idEspace` int(11) NOT NULL,
  `idService` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `lienESB`
--

INSERT INTO `lienESB` (`id`, `idEspace`, `idService`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 1);

-- --------------------------------------------------------

--
-- Structure de la table `lienESR`
--

CREATE TABLE `lienESR` (
  `id` int(11) NOT NULL,
  `idEspace` int(11) NOT NULL,
  `idService` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `lienESR`
--

INSERT INTO `lienESR` (`id`, `idEspace`, `idService`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 1),
(4, 4, 2);

-- --------------------------------------------------------

--
-- Structure de la table `lienSRB`
--

CREATE TABLE `lienSRB` (
  `id` int(11) NOT NULL,
  `idService` int(11) NOT NULL,
  `idResaBureau` int(11) NOT NULL,
  `quantite` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `lienSRB`
--

INSERT INTO `lienSRB` (`id`, `idService`, `idResaBureau`, `quantite`) VALUES
(21, 2, 30, 1),
(22, 2, 30, 3),
(23, 1, 31, 3),
(29, 1, 33, 4),
(30, 1, 33, 3),
(31, 2, 42, 4),
(32, 1, 43, 4),
(33, 1, 47, 0),
(34, 1, 48, 1);

-- --------------------------------------------------------

--
-- Structure de la table `lienSRR`
--

CREATE TABLE `lienSRR` (
  `id` int(11) NOT NULL,
  `idService` int(11) NOT NULL,
  `idResaReunion` int(11) NOT NULL,
  `quantite` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `lienSRR`
--

INSERT INTO `lienSRR` (`id`, `idService`, `idResaReunion`, `quantite`) VALUES
(96, 2, 83, 8),
(97, 1, 84, 3),
(99, 1, 85, 3),
(101, 1, 86, 10),
(102, 2, 90, 1),
(103, 1, 91, 0),
(104, 2, 91, 0),
(105, 1, 92, 2),
(106, 2, 92, 1);

-- --------------------------------------------------------

--
-- Structure de la table `reservationsBureau`
--

CREATE TABLE `reservationsBureau` (
  `id` int(11) NOT NULL,
  `idClient` int(11) NOT NULL,
  `idEspace` int(11) NOT NULL,
  `dateDebut` date DEFAULT NULL,
  `dateFin` varchar(255) DEFAULT NULL,
  `payer` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `reservationsBureau`
--

INSERT INTO `reservationsBureau` (`id`, `idClient`, `idEspace`, `dateDebut`, `dateFin`, `payer`) VALUES
(37, 31, 2, '2016-02-03', NULL, 0),
(38, 32, 2, '2016-02-03', NULL, 0),
(39, 0, 2, '2016-02-03', NULL, 0),
(40, 0, 2, '2016-02-03', NULL, 0),
(41, 0, 2, '2016-02-03', NULL, 0),
(42, 36, 2, '2017-02-04', NULL, 0),
(43, 36, 2, NULL, NULL, 0),
(44, 37, 2, '2017-04-10', NULL, 0),
(45, 37, 2, NULL, NULL, 0),
(46, 37, 2, NULL, NULL, 0),
(47, 36, 2, NULL, NULL, 0),
(48, 50, 3, '2017-08-24', NULL, 0);

-- --------------------------------------------------------

--
-- Structure de la table `reservationsReunion`
--

CREATE TABLE `reservationsReunion` (
  `id` int(11) NOT NULL,
  `idClient` int(11) NOT NULL,
  `idEspace` int(11) NOT NULL,
  `HeureDebut` int(11) DEFAULT NULL,
  `HeureFin` int(11) DEFAULT NULL,
  `payer` tinyint(1) NOT NULL DEFAULT '0',
  `dateResa` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `reservationsReunion`
--

INSERT INTO `reservationsReunion` (`id`, `idClient`, `idEspace`, `HeureDebut`, `HeureFin`, `payer`, `dateResa`) VALUES
(85, 37, 1, 14, 16, 0, '8/10/2009'),
(86, 37, 2, 9, 11, 0, '27/10/2018'),
(87, 37, 1, 8, 8, 0, '24/10/2009'),
(88, 37, 1, 8, 8, 0, '24/10/2009'),
(89, 37, 1, 8, 8, 0, '24/10/2009'),
(90, 36, 4, 17, 18, 0, '23/10/2018'),
(91, 36, 1, 8, 8, 0, '24/10/2009'),
(92, 26, 1, 8, 8, 0, '8/10/2009');

-- --------------------------------------------------------

--
-- Structure de la table `salleReunions`
--

CREATE TABLE `salleReunions` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `milieu` varchar(255) NOT NULL,
  `lieu` varchar(255) NOT NULL,
  `nbPersonnes` int(11) NOT NULL,
  `prix` float NOT NULL,
  `lienIMG` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `salleReunions`
--

INSERT INTO `salleReunions` (`id`, `nom`, `milieu`, `lieu`, `nbPersonnes`, `prix`, `lienIMG`, `description`) VALUES
(1, 'reunionA', '16eme', 'rue Suchet', 10, 500, 'img/reunion/reunionA.jpg', 'Cet espace dispose de x bureaux neufs, avec fauteuils super confort etc etc. '),
(2, 'reunionB', '1er', 'boulevard truc', 40, 10000.5, 'img/reunion/reunionB.jpg', 'Je ne connais pas cet espace mais la photo est tres belle meme si elle vient du net lol'),
(4, 'reunionC', '16eme', 'rue suchet', 20, 100, 'img/reunion/reunionC.jpg', '');

-- --------------------------------------------------------

--
-- Structure de la table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prix` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `services`
--

INSERT INTO `services` (`id`, `nom`, `prix`) VALUES
(1, 'café', 2),
(2, 'afterwork', 10);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `bureau`
--
ALTER TABLE `bureau`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `compte`
--
ALTER TABLE `compte`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mail` (`mail`);

--
-- Index pour la table `lienESB`
--
ALTER TABLE `lienESB`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `lienESR`
--
ALTER TABLE `lienESR`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `lienSRB`
--
ALTER TABLE `lienSRB`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `lienSRR`
--
ALTER TABLE `lienSRR`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `reservationsBureau`
--
ALTER TABLE `reservationsBureau`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `reservationsReunion`
--
ALTER TABLE `reservationsReunion`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `salleReunions`
--
ALTER TABLE `salleReunions`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `bureau`
--
ALTER TABLE `bureau`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `compte`
--
ALTER TABLE `compte`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT pour la table `lienESB`
--
ALTER TABLE `lienESB`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `lienESR`
--
ALTER TABLE `lienESR`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `lienSRB`
--
ALTER TABLE `lienSRB`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT pour la table `lienSRR`
--
ALTER TABLE `lienSRR`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;
--
-- AUTO_INCREMENT pour la table `reservationsBureau`
--
ALTER TABLE `reservationsBureau`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT pour la table `reservationsReunion`
--
ALTER TABLE `reservationsReunion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;
--
-- AUTO_INCREMENT pour la table `salleReunions`
--
ALTER TABLE `salleReunions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
