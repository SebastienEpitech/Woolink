<?php
	session_start();
	include_once("fonctions.php");
	if (isset($_SESSION['User'])){
      $mail = $_SESSION['User'];
  }
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Woolink</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href=" bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href=" dist/css/AdminLTE.css">
  <!-- iCheck -->
  <link rel="stylesheet" href=" plugins/iCheck/square/blue.css">
  <link rel="stylesheet" href="calendar.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
  <?php
$conn = connexion();
mysqli_set_charset($conn, 'utf8');
  if (isset($_POST["nomSociete"]) &&  isset($_POST["siret"])  && isset($_POST["nomRepresentant"])
    && isset($_POST["prenomRepresentant"]) && isset($_POST["tel"]) ){
      $nomS =  $_POST["nomSociete"];
      $siret =  $_POST["siret"];
      $nomR = $_POST["nomRepresentant"];
      $prenomR = $_POST["prenomRepresentant"];
      $tel = $_POST["tel"];
      $sql="UPDATE compte SET nomSociete='$nomS', siret='$siret', nomRepresentant='$nomR', prenomRepresentant='$prenomR', tel='$tel' WHERE mail='$mail'";
      $result = mysqli_query($conn, $sql);
      header("Status: 301 Moved Permanently", false, 301);
      header("Location: http://localhost:7800/espaceClient/mdp.php");
      exit();

  }
 mysqli_close($conn);
  		?>
<div class="login-box">
  <div class="login-logo">
    <img src="entete.png" width="100%" />
    <!-- <a href="../../index2.html"><b>Admin</b>LTE</a> -->
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">

  <form action="suivant.php" method="post">
<?php
$conn = connexion();
mysqli_set_charset($conn, 'utf8');
$sql= "SELECT nomSociete, siret, mail, dateDebut, nomRepresentant, prenomRepresentant, tel, categorie FROM compte WHERE mail='$mail'";
$result= mysqli_query($conn, $sql);
while ($data = mysqli_fetch_array($result)) {
    echo "Nom de la société :
      <div class='form-group has-feedback' style='color:black;'>
        <input type='text' class='form-control' name='nomSociete' value=".$data['nomSociete'].">
      </div>
      Numéro de siret :
      <div class='form-group has-feedback' style='color:black;'>
      <input type='text' class='form-control' name='siret' value=".$data['siret'].">
      </div>
      Représentant :
      <div class='form-group has-feedback' style='color:black;'>
      <input type='text' class='form-control' name='nomRepresentant' value=".$data['nomRepresentant'].">
      <input type='text' class='form-control' name='prenomRepresentant' value=".$data['prenomRepresentant'].">
      </div>
      E-Mail :
      <div class='form-group has-feedback' style='color:black;'>
      ".$data['mail']."
      </div>
      Téléphone :
      <div class='form-group has-feedback' style='color:black;'>
      <input type='text' class='form-control' name='tel' value=".$data['tel'].">
      </div>
      Nom du bureau occupé :
      <div class='form-group has-feedback' style='color:black;'>";
      $req2 = "SELECT nom FROM bureau WHERE id IN( SELECT idEspace FROM reservationsBureau WHERE idClient IN (SELECT id FROM compte WHERE mail='$mail'))";
      mysqli_set_charset($conn, 'utf8');
      $res2 = mysqli_query($conn, $req2);
      while ($data2 = mysqli_fetch_assoc($res2)) {
        echo $data2['nom'];
            }
      echo "</div>
      Date de début :
      <div class='form-group has-feedback' style='color:black;'>
        ".$data['dateDebut']."
      </div>
      Catégorie :
      <div class='form-group has-feedback' style='color:black;'>
        ".$data['categorie']."
      </div>
      Prix :
      <div class='form-group has-feedback' style='color:black;'>";
      $req3 = "SELECT prix, garantie FROM bureau WHERE id IN( SELECT idEspace FROM reservationsBureau WHERE idClient IN (SELECT id FROM compte WHERE mail='$mail'))";
      mysqli_set_charset($conn, 'utf8');
      $res3 = mysqli_query($conn, $req3);
      while ($data3 = mysqli_fetch_assoc($res3)) {
        echo $data3['prix']."
      </div>
      Depot de garantie
      <div class='form-group has-feedback' style='color:black;'>
        ".$data3['garantie']."
      </div>
      ";
    }
}
?>

      <div class="row">
        <div class="bouton" >
          <button type="submit" class="btn btn-primary btn-block btn-flat">Suivant</button>
        </div>
        <!-- /.col -->
      </div>
</form>

    <!-- <a href="#">I forgot my password</a><br>
    <a href="register.html" class="text-center">Register a new membership</a> -->

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src=" plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src=" bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src=" plugins/iCheck/icheck.min.js"></script>
<!-- <script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script> -->
<script type="text/javascript" src="mCalandar.js"></script>
</body>
</html>
