<?php
	session_start();
	include_once("fonctions.php");
	if (isset($_SESSION['id'])){
      $idEspace = $_SESSION['id'];
  }

  if (isset($_SESSION['User'])){
      $mail = $_SESSION['User'];
      $conn = connexion();
      $req ="SELECT id FROM compte WHERE mail='$mail'";
      mysqli_set_charset($conn, 'utf8');
      $res = mysqli_query($conn, $req);
      while ($data = mysqli_fetch_assoc($res)) {
        $idClient = $data['id'];
      }
   }

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Modals</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href=" bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href=" dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href=" dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <script>
    function valide(){
      alert("Vous avez bien enregistré cet espace");
    }
  </script>
  <style>
    .example-modal .modal {
      position: relative;
      top: auto;
      bottom: auto;
      right: auto;
      left: auto;
      display: block;
      z-index: 1;
    }

    .example-modal .modal {
      background: transparent !important;
    }

    .bouton{
    color:#0000ff;
    font-size:24px;
    cursor:pointer;
    }
    .bouton:hover{
      text-decoration:underline;
    }
    .texte{
      border:1px solid #333333;
      background:#eeeeee;
      padding:10px;
      color:#333333;
    }
    .texte:hover{
      border:1px solid #000000;
      background:#cccccc;
      color:#000000;
    }
  </style>

</head>
<body class="hold-transition skin-blue sidebar-mini">

  <?php
//il a enregisté l'espace
    if (isset($_POST['bouton'])){
			$nb=$_POST['nb'];


      $conn = connexion();
			$req = "INSERT INTO reservationsBureau (idClient, idEspace) VALUES ('$idClient', '$idEspace')";
      mysqli_set_charset($conn, 'utf8');
      $res = mysqli_query($conn, $req);
			$idresa = mysqli_insert_id($conn);
			for($i=0;$i<$nb; $i++)
      {
				$idService =$_POST['id'.$i];
				$quantite = $_POST['nombre'.$i];
				$req2 = "INSERT INTO lienSRB (idService, idResaBureau, quantite) VALUES ('$idService', '$idresa', '$quantite')";
				mysqli_set_charset($conn, 'utf8');
				$res2 = mysqli_query($conn, $req2);
      }
			header("Status: 301 Moved Permanently", false, 301);
			header("Location: http://localhost:7800/espaceClient/datesResa.php");
			mysqli_close($conn);
				}

  include_once("header.php");
  ?>


<div class="wrapper">

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Enregistrer votre bureau
        <small>new</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Accueil</a></li>
        <li class="active">Enregistrer votre bureau</li>
      </ol>
    </section>

    <!-- Main content -->
    <div id="listOffres" class="content"  >

      <?php
        $conn = connexion();
        $req = "SELECT * FROM bureau WHERE id='$idEspace'"; //WHERE milieu='$milieu'
        mysqli_set_charset($conn, 'utf8');
        $res = mysqli_query($conn, $req);
        while ($data = mysqli_fetch_assoc($res)) {
          // $id=$data['id'];
          echo "
          <div class='example-modal'>
            <div class='modal'>
              <div class='modal-dialog'>
                <div class='modal-content'>
                  <div class='modal-header'>
                    <h4 class='modal-title'>".$data['nom']."</h4>
                  </div>
                  <div class='modal-body'>
                    <div id='gaucheResa'>
                      <img src=".$data['lienIMG']." height='40%' width='40%' />
                    </div> <!--gauche-->
                    <div id='droiteResa'>
                    milieu  ".$data['milieu']." </br>
                    lieu    ".$data['lieu']." </br>
                    nombre max de personnes  ".$data['nbPersonnes']." </br>
                    prix :  ".$data['prix']."€ </br>
                    Description : ".$data['description']."</br>";

                       }
									 echo "<form action='offreBureau.php' method='POST'>";
									$req2 = "SELECT * FROM services WHERE id IN (SELECT idService FROM lienESB WHERE idEspace='$idEspace')";
									$res2 = mysqli_query($conn, $req2);
									mysqli_set_charset($conn, 'utf8');
									$i=0;
									$nbrprod = mysqli_num_rows($res2);
									while ($data = mysqli_fetch_assoc($res2)) {
					          // $id=$data['id'];
					          echo $data['nom']."<input type='hidden' name='id".$i."' value='".$data['id']."' /><input type='number' id='nombre' name='nombre".$i."' value='0'/> </br>";
										$i++;
								 	}
				                   mysqli_close($conn);
													 echo "<input type='hidden' name='nb' value='".$nbrprod."' />"
													 ?>
												 	<input type ='submit' name='bouton' value='Réserver cette salle'/>
												 </form>
                    </div> <!--droite-->
                  </div>
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
          </div>
          <!-- /.example-modal -->


      <!-- /.example-modal -->
    </div>
    <!-- /.content -->
  	</div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.8
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>

      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-yellow"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                <p>New phone +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                <p>nora@example.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-file-code-o bg-green"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                <p>Execution time 5 seconds</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Resume
                <span class="label label-success pull-right">95%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Laravel Integration
                <span class="label label-warning pull-right">50%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Back End Framework
                <span class="label label-primary pull-right">68%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Other sets of options are available
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Allow the user to show his name in blog posts
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Chat Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Delete chat history
              <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src=" plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src=" bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src=" plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src=" dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src=" dist/js/demo.js"></script>
</body>
</html>
