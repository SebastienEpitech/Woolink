<?php
	session_start();
	include_once("fonctions.php");
	if (isset($_SESSION['id'])){
      $idEspace = $_SESSION['id'];
  }

  if (isset($_SESSION['User'])){
      $mail = $_SESSION['User'];
      $conn = connexion();
      $req ="SELECT id FROM compte WHERE mail='$mail'";
      mysqli_set_charset($conn, 'utf8');
      $res = mysqli_query($conn, $req);
      while ($data = mysqli_fetch_assoc($res)) {
        $idClient = $data['id'];
      }
   }

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Woolink</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="calendar.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
  </script>
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href=" bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href=" dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href=" dist/css/skins/_all-skins.min.css">
	<style>
	    .example-modal .modal {
	      position: relative;
	      top: auto;
	      bottom: auto;
	      right: auto;
	      left: auto;
	      display: block;
	      z-index: 1;
	    }

	    .example-modal .modal {
	      background: transparent !important;
	    }

	    .bouton{
	    color:#0000ff;
	    font-size:24px;
	    cursor:pointer;
	    }
	    .bouton:hover{
	      text-decoration:underline;
	    }
	    .texte{
	      border:1px solid #333333;
	      background:#eeeeee;
	      padding:10px;
	      color:#333333;
	    }
	    .texte:hover{
	      border:1px solid #000000;
	      background:#cccccc;
	      color:#000000;
	    }
	  </style>


</head>
<body class="hold-transition skin-blue sidebar-mini">
	<?php
$nomS =  $_POST["nomSociete"];
$siret =  $_POST["siret"];
$mail =  $_POST["mail"];
$nomR = $_POST["nomRepresentant"];
$prenomR = $_POST["prenomRepresentant"];
$tel = $_POST["tel"];

$categorie = $_POST["categorie"];
$bureau = $_POST["bureau"];
$debut = $_POST["debut"];
$prix = $_POST["prix"];
$garantie = $_POST["garantie"];

$conn = connexion();
mysqli_set_charset($conn, 'utf8');

  if (isset($_POST["nomSociete"]) &&  isset($_POST["siret"]) && isset($_POST["mail"])  && isset($_POST["nomRepresentant"])
    && isset($_POST["prenomRepresentant"]) && isset($_POST["tel"]) && isset($_POST["categorie"]) && isset($_POST["bureau"]) && isset($_POST["debut"]) && isset($_POST["prix"]) && isset($_POST["garantie"]) ){
       $sql = "INSERT INTO compte (nomSociete, siret, mail, dateDebut, nomRepresentant, prenomRepresentant, tel, categorie) VALUES ('$nomS', '$siret', '$mail',  '$debut', '$nomR', '$prenomR', '$tel', '$categorie')";
       $result = mysqli_query($conn, $sql);
       $idDernier = mysqli_insert_id($conn);
			 echo ("bureau :".$bureau);
       $sql2= "SELECT id FROM bureau WHERE nom='$bureau'";
       $result2= mysqli_query($conn, $sql2);
			 echo ("bureau :".$bureau); //NE MARCHE PAS !!!!!!!!!!!!!!!!!!!!!!
       while ($data2 = mysqli_fetch_array($result2)) {
          $idBureau= $data2['id'];

			  echo("idbureau : ".$iBureau);
       $sql3 = "INSERT INTO reservationsBureau (idClient, dateDebut, idEspace) VALUES  ('$idDernier', '$debut', '$idBureau')";
       $result3= mysqli_query($conn, $sql3);
			 echo("iddernier : ".$idDernier);
			 $msg= "Bonjour, Pour finaliser l'inscription, veuillez cliquer sur ce lien : http://localhost:7800/espaceClient/code.php Votre numéro est".$idDernier." . Cordialement";
			 if ($idDernier!= 0){
			 mail($mail, "Inscription", $msg);
		 }else {
		 	echo "mail déjà utilisé";
		 }
  }
	$sql = "UPDATE bureau SET prix='$prix' , garantie='$garantie' WHERE nom='$bureau'";
	$result = mysqli_query($conn, $sql);
}
 mysqli_close($conn);

  		?>
  <?php

  include_once("headerAdmin.php");
  ?>


<div class="wrapper">

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Espace administrateur
      </h1>
    </section>

    <!-- Main content -->
    <!-- <div class="content"  > -->


			<section class="content" >
				<form action="admin.php" method="POST">
  			<div class="box box-default">
					<div class="box-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Nom de la société : <span class="required">*</span></label>
									<input type="text" name="nomSociete" class="field-long" required/>
								</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label>Numéro Siret : <span class="required">*</span></label>
								<input type="text" name="siret" class="field-long" required/>
							</div>
							<div class="form-group">
								<label>Représentant : <span class="required">*</span></label>
								<input type="text" name="nomRepresentant" class="field-long" placeholder="Nom" requiered />
								<input type="text" name="prenomRepresentant" class="field-long" placeholder="Prénom" required /></li>
							</div>
							<div class="form-group">
								<label>Email <span class="required">*</span></label>
								<input type="email" name="mail" class="field-long" required />
								<div style="display:none;">E-mail déjà utilisée</div>
							</div>
							<!-- <div class="form-group">
								<label>Mot de passe <span class="required">*</span></label>
								<input type="password" name="mdp" class="field-long" required />
							</div> -->
							<div class="form-group">
								<label>Téléphone : <span class="required">*</span></label>
								<input type="text" name="tel" class="field-long" required/>
							</div>
							<div class="form-group">
								<label>Nom du bureau occupé : <span class="required">*</span></label>
								<input type="text" name="bureau" class="field-long" required/>
							</div>
							<div class="form-group">
								<label>Date de début : <span class="required">*</span></label>
								<input type="text" class="mCalendar" name="debut" value="24/10/2017">
							</div>
							<div class="form-group">
								<label>Prix : <span class="required">*</span></label>
								<input type="text" name="prix" class="field-long" required/>
							</div>
							<div class="form-group">
								<label>Dépot de garantie : <span class="required">*</span></label>
								<input type="text" name="garantie" class="field-long" required/>
							</div>
							<div class="form-group">
								<label>Catégorie :<span class="required">*</span></label>
								<select class="form-control select2" style="width: 100%;" name="categorie" >
										<option value="informatique" >Informatique</option>
										<option value="commerce" >Commerce</option>
										<option value="medical">Médical</option>
								</select>
							</div>
							<div class="form-group">
								<input type="submit" name="bouton" value='Envoyer'  />
							</div>
							<!-- /.form-group -->
						</div>
						<!-- /.col -->
					</div>
					<!-- /.row -->
				</div>
			</div>
			<!-- <label>Date de début : <span class="required">*</span></label>
			<input type="text" name="debut" class="field-long" required/> -->
				<!-- <form action="inscription_connexion.php" method="POST">
					<ul class="form-style-1">


					</ul>
				</form> -->
			</section>


      <!-- /.example-modal -->
    </div>
    <!-- /.content -->
  	</div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.8
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>

      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-yellow"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                <p>New phone +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                <p>nora@example.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-file-code-o bg-green"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                <p>Execution time 5 seconds</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Resume
                <span class="label label-success pull-right">95%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Laravel Integration
                <span class="label label-warning pull-right">50%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Back End Framework
                <span class="label label-primary pull-right">68%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Other sets of options are available
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Allow the user to show his name in blog posts
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Chat Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Delete chat history
              <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src=" plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src=" bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src=" plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src=" dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src=" dist/js/demo.js"></script>
<script type="text/javascript" src="mCalandar.js"></script>

</body>
</html>
