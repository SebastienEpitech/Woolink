<?php
    include("fonctions.php");
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="style.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <?php
setlocale(LC_TIME, 'fra_fra');
$today = date("Y-m-d");
$nomS =  $_POST["nomSociete"];
$siret =  $_POST["siret"];
$mail =  $_POST["mail"];
$mdp= $_POST["mdp"];
$nomR = $_POST["nomRepresentant"];
$prenomR = $_POST["prenomRepresentant"];
$tel = $_POST["tel"];
$desc = $_POST["area"];
$categorie = $_POST["categorie"];
$bureau = $_POST["bureau"];
$debut = $_POST["debut"];

$bouton = $_POST["bouton"];
$conn = connexion();
mysqli_set_charset($conn, 'utf8');
if ($bouton=="Inscription"){
  if (isset($_POST["nomSociete"]) &&  isset($_POST["siret"]) && isset($_POST["mail"]) && isset($_POST["mdp"]) && isset($_POST["nomRepresentant"])
    && isset($_POST["prenomRepresentant"]) && isset($_POST["tel"]) && isset($_POST["categorie"]) && isset($_POST["bureau"]) && isset($_POST["debut"]) ){
       $sql = "INSERT INTO compte (nomSociete, siret, mail, mdp, dateDebut, nomRepresentant, prenomRepresentant, tel, categorie, description) VALUES ('$nomS', '$siret', '$mail', '$mdp', '$today', '$nomR', '$prenomR', '$tel', '$categorie', '$desc')";
       $result = mysqli_query($conn, $sql);
       $idDernier = mysqli_insert_id($conn);
       $sql2= "SELECT id FROM bureau WHERE nom='$bureau'";
       $result2= mysqli_query($conn, $sql2);
       while ($data = mysqli_fetch_array($result2)) {
          $idBureau= $data['id'];
       }
       $sql3 = "INSERT INTO reservationsBureau (idClient, dateDebut, idEspace) VALUES  ('$idDernier', '$debut', '$idBureau')";
       $result3= mysqli_query($conn, $sql3);

       session_start();
       $_SESSION['User']=$mail;
       header("Status: 301 Moved Permanently", false, 301);
       header("Location: http://localhost:7800/espaceClient/profil.php");

  }
}else {
    $sql="SELECT mail, mdp FROM compte ";
    $result = mysqli_query($conn, $sql);
    $bool=false;
    while ($data = mysqli_fetch_array($result)) {
      // on affiche les résultats
      if ($data['mail'] == $mail && $data['mdp']== $mdp){
            $bool=true;
            session_start();
            $_SESSION['User']=$mail;
            header("Status: 301 Moved Permanently", false, 301);
            header("Location: http://localhost:7800/espaceClient/profil.php");
            exit();
       }
           }
         if ($bool==false) {
           echo "<div class='faux'> <strong> Email ou mot de passe incorrect </strong> </div>";
         }
       }
 mysqli_close($conn);



  		?>
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-lg"><b>A</b>LT</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <div class="navbar-custom-menu">
      </div>
    </nav>
  </header>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <div id="gauche">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Inscrivez vous :
          </h1>

        </section>
        <section class="content" >
          <form action="inscription_connexion.php" method="POST">
            <ul class="form-style-1">
              <li>
                  <label>Nom de la société : <span class="required">*</span></label>
                  <input type="text" name="nomSociete" class="field-long" required/>
              </li>
              <li>
                  <label>Numéro Siret : <span class="required">*</span></label>
                  <input type="text" name="siret" class="field-long" required/>
              </li>
              <li>
                <label>Représentant : <span class="required">*</span></label>
                <input type="text" name="nomRepresentant" class="field-long" placeholder="Nom" requiered />
              </br></br><input type="text" name="prenomRepresentant" class="field-long" placeholder="Prénom" required /></li>
              <li>
                <label>Email <span class="required">*</span></label>
                <input type="email" name="mail" class="field-long" required />
                <div style="display:none;">E-mail déjà utilisée</div>
              </li>
              <li>
                <label>Mot de passe <span class="required">*</span></label>
                <input type="password" name="mdp" class="field-long" required />
              </li>
              <li>
                  <label>Téléphone : <span class="required">*</span></label>
                  <input type="text" name="tel" class="field-long" required/>
              </li>
              <li>
                  <label>Nom du bureau occupé : <span class="required">*</span></label>
                  <input type="text" name="bureau" class="field-long" required/>
              </li>
              <li>
                  <label>Date de début : <span class="required">*</span></label>
                  <input type="text" name="debut" class="field-long" required/>
              </li>
              <li>
                <label>Catégorie<span class="required">*</span></label>
                <select name="categorie" >
                    <option value="informatique" >Informatique</option>
                    <option value="commerce" >Commerce</option>
                    <option value="medical">Médical</option>
                </select>
              </li>
              <li>
                <label>Description de l'entreprise  </label>
                <textarea name="area" id="field5" class="field-long field-textarea"></textarea>
              </li>
              <li>
                <input type="submit" name="bouton" value='Inscription' onClick="onteste($_POST['mdp'])" />
              </li>
            </ul>
          </form>
        </section>
    </div>
    <div id="droite">
      <section class="content-header">
        <h1>
          Connectez vous :
        </h1>

      </section>
      <section class="content" >
        <form action="inscription_connexion.php" method="POST">
          <ul class="form-style-1">
            <li>
              <label>Email <span class="required">*</span></label>
              <input type="email" name="mail" class="field-long" required />
            </li>
            <li>
              <label>Mot de passe <span class="required">*</span></label>
              <input type="password" name="mdp" class="field-long" required />
            </li>
            <li>
              <input type="submit" name="bouton" value='Connexion' onClick="onteste($_POST['mdp'])" />
            </li>
          </ul>
        </form>
      </section>
    </div>
  </div>
  <!-- /.content-wrapper -->
<script src="jquery-2.2.3.min.js"></script>
<script>
$("#mail").focusout(function() {
    var mail = $("#mail").val();
    $.post("checkEmail.php", { email: mail}).done(function( data ) {
    if (data == "false"){
      $("#mailError").show();
    }
    else {
      $("#mailError").hide();
    }
  });
  })

</script>


<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b>Version</b> 2.3.8
  </div>
  <strong>Copyright &copy; 2014-2016 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights
  reserved.
</footer>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
  <!-- Create the tabs -->
  <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
    <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
    <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
  </ul>
  <!-- Tab panes -->
  <div class="tab-content">
    <!-- Home tab content -->
    <div class="tab-pane" id="control-sidebar-home-tab">
      <h3 class="control-sidebar-heading">Recent Activity</h3>
      <ul class="control-sidebar-menu">
        <li>
          <a href="javascript:void(0)">
            <i class="menu-icon fa fa-birthday-cake bg-red"></i>

            <div class="menu-info">
              <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

              <p>Will be 23 on April 24th</p>
            </div>
          </a>
        </li>
        <li>
          <a href="javascript:void(0)">
            <i class="menu-icon fa fa-user bg-yellow"></i>

            <div class="menu-info">
              <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

              <p>New phone +1(800)555-1234</p>
            </div>
          </a>
        </li>
        <li>
          <a href="javascript:void(0)">
            <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

            <div class="menu-info">
              <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

              <p>nora@example.com</p>
            </div>
          </a>
        </li>
        <li>
          <a href="javascript:void(0)">
            <i class="menu-icon fa fa-file-code-o bg-green"></i>

            <div class="menu-info">
              <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

              <p>Execution time 5 seconds</p>
            </div>
          </a>
        </li>
      </ul>
      <!-- /.control-sidebar-menu -->

      <h3 class="control-sidebar-heading">Tasks Progress</h3>
      <ul class="control-sidebar-menu">
        <li>
          <a href="javascript:void(0)">
            <h4 class="control-sidebar-subheading">
              Custom Template Design
              <span class="label label-danger pull-right">70%</span>
            </h4>

            <div class="progress progress-xxs">
              <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
            </div>
          </a>
        </li>
        <li>
          <a href="javascript:void(0)">
            <h4 class="control-sidebar-subheading">
              Update Resume
              <span class="label label-success pull-right">95%</span>
            </h4>

            <div class="progress progress-xxs">
              <div class="progress-bar progress-bar-success" style="width: 95%"></div>
            </div>
          </a>
        </li>
        <li>
          <a href="javascript:void(0)">
            <h4 class="control-sidebar-subheading">
              Laravel Integration
              <span class="label label-warning pull-right">50%</span>
            </h4>

            <div class="progress progress-xxs">
              <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
            </div>
          </a>
        </li>
        <li>
          <a href="javascript:void(0)">
            <h4 class="control-sidebar-subheading">
              Back End Framework
              <span class="label label-primary pull-right">68%</span>
            </h4>

            <div class="progress progress-xxs">
              <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
            </div>
          </a>
        </li>
      </ul>
      <!-- /.control-sidebar-menu -->

    </div>
    <!-- /.tab-pane -->
    <!-- Stats tab content -->
    <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
    <!-- /.tab-pane -->
    <!-- Settings tab content -->
    <div class="tab-pane" id="control-sidebar-settings-tab">
      <form method="post">
        <h3 class="control-sidebar-heading">General Settings</h3>

        <div class="form-group">
          <label class="control-sidebar-subheading">
            Report panel usage
            <input type="checkbox" class="pull-right" checked>
          </label>

          <p>
            Some information about this general settings option
          </p>
        </div>
        <!-- /.form-group -->

        <div class="form-group">
          <label class="control-sidebar-subheading">
            Allow mail redirect
            <input type="checkbox" class="pull-right" checked>
          </label>

          <p>
            Other sets of options are available
          </p>
        </div>
        <!-- /.form-group -->

        <div class="form-group">
          <label class="control-sidebar-subheading">
            Expose author name in posts
            <input type="checkbox" class="pull-right" checked>
          </label>

          <p>
            Allow the user to show his name in blog posts
          </p>
        </div>
        <!-- /.form-group -->

        <h3 class="control-sidebar-heading">Chat Settings</h3>

        <div class="form-group">
          <label class="control-sidebar-subheading">
            Show me as online
            <input type="checkbox" class="pull-right" checked>
          </label>
        </div>
        <!-- /.form-group -->

        <div class="form-group">
          <label class="control-sidebar-subheading">
            Turn off notifications
            <input type="checkbox" class="pull-right">
          </label>
        </div>
        <!-- /.form-group -->

        <div class="form-group">
          <label class="control-sidebar-subheading">
            Delete chat history
            <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
          </label>
        </div>
        <!-- /.form-group -->
      </form>
    </div>
    <!-- /.tab-pane -->
  </div>
</aside>
<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
$.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
</body>
</html>
