<?php
    include("fonctions.php");
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Woolink</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/square/blue.css">
  <link rel="stylesheet" href="calendar.css">
  <link rel="stylesheet" href="style.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
  <?php

$conn = connexion();
mysqli_set_charset($conn, 'utf8');
if (isset($_POST["mail"]) && isset($_POST["mdp"])){
  $mail =  $_POST["mail"];
  $mdp= $_POST["mdp"];
    $sql="SELECT mail, mdp FROM compte WHERE mail='$mail' AND mdp='$mdp'";
    $result = mysqli_query($conn, $sql);
    if (mysqli_fetch_array($result)!= NULL) {
            session_start();
            if ($mail=='woolink@admin.com' && $mdp=='woolink1234'){
              $_SESSION['User']=$mail;
              header("Status: 301 Moved Permanently", false, 301);
              header("Location: http://localhost:7800/espaceClient/admin.php");
              exit();
            }else{

            $_SESSION['User']=$mail;
            header("Status: 301 Moved Permanently", false, 301);
            header("Location: http://localhost:7800/espaceClient/profil.php");
            exit();
          }
    }else{
      echo "<script type='texte/javascript'> var hello = 'helloworld' ; alert(hello);</script>" ;
    }
  }

 mysqli_close($conn);

  		?>
<div class="login-box">
  <div class="login-logo">
    <img src="entete.png" width="100%" />
    <!-- <a href="../../index2.html"><b>Admin</b>LTE</a> -->

  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
<h2 class="centrer">Espace client </h2>
    <form action="connexion.php" method="post">
      E-Mail *
      <div class="form-group has-feedback">
        <input type="email" class="form-control" name="mail" required>
      </div>
      Mot de passe *
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="mdp" required>
        <a href="../contact_us.html" class="mdpPerdu">Mot de passe perdu ? Contactez-nous</a>
      </div>
      <div class="row">
        <div class="bouton" >
          <button type="submit" class="btn btn-primary btn-block btn-flat">Connexion</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <!-- <a href="#">I forgot my password</a><br>
    <a href="register.html" class="text-center">Register a new membership</a> -->

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="plugins/iCheck/icheck.min.js"></script>
<!-- <script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script> -->
<script type="text/javascript" src="mCalandar.js"></script>
</body>
</html>
