<?php
	session_start();
	include_once("fonctions.php");
	if (isset($_SESSION['User'])){
      $mail = $_SESSION['User'];
  }
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Woolink</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
	<link rel="stylesheet" href="style.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<?php
	$conn = connexion();
	$req = "SELECT nomSociete, id FROM compte WHERE mail='$mail'";
	mysqli_set_charset($conn, 'utf8');
	$res = mysqli_query($conn, $req);
	while ($data = mysqli_fetch_assoc($res)) {
		$user=$data['nomSociete'];
		$idClient=$data['id'];

				}
		mysqli_close($conn);
include_once("header.php");
 ?>
<div class="wrapper">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
		<section class="content-header">
      <h1>
        Profil
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Accueil</a></li>
        <li class="active">Profil</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Main row -->
      <div class="row">
				<!-- START CAROUSEL - Prolil-->

				<div class="row">
					<div class="col-md-6">
						<div class="box box-solid">
							<div class="box-header with-border">
								<h3 class="box-title">Pub Woolink</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
									<ol class="carousel-indicators">
										<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
										<li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
										<li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
									</ol>
									<div class="carousel-inner">
										<div class="item active">
											<img src="http://placehold.it/900x500/39CCCC/ffffff&text=I+Love+Bootstrap" alt="First slide">

											<div class="carousel-caption">
												First Slide
											</div>
										</div>
										<div class="item">
											<img src="http://placehold.it/900x500/3c8dbc/ffffff&text=I+Love+Bootstrap" alt="Second slide">

											<div class="carousel-caption">
												Second Slide
											</div>
										</div>
										<div class="item">
											<img src="http://placehold.it/900x500/f39c12/ffffff&text=I+Love+Bootstrap" alt="Third slide">

											<div class="carousel-caption">
												Third Slide
											</div>
										</div>
									</div>
									<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
										<span class="fa fa-angle-left"></span>
									</a>
									<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
										<span class="fa fa-angle-right"></span>
									</a>
								</div>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<div class="col-md-4">
						<div class="box box-solid">
							<!-- <div class="box-header with-border">
								<h3 class="box-title">Profil</h3>
							</div> -->
							<!-- /.box-header -->
							<div class="box-body profil">
								<?php
									$conn = connexion();
									$req = "SELECT * FROM compte WHERE mail='$mail'";
								 	mysqli_set_charset($conn, 'utf8');
									$res = mysqli_query($conn, $req);
									while ($data = mysqli_fetch_assoc($res)) {
										echo "
										<h2 class='centrer'> Société ''<span class='titre'>".$data['nomSociete']."</span>'' </h2>
										<p class='siret'>Siret : ".$data['siret']." </p>

												<div></br>
												<p>  <img src='img/picto/Bonhomme.png' width='10%'/>
													Représenté par <span class='nom'>".$data['nomRepresentant']." ".$data['prenomRepresentant']."</span>
													</p>
													<p><img src='img/picto/mail.png' width='10%'/> Mail : ".$data['mail']."
													</p>
													<p><img src='img/picto/tel.png' width='10%'/> Tel : ".$data['tel']."
													</p>
													<p><img src='img/picto/categorie.png' width='10%'/> Catégorie : ".$data['categorie']."
													</br> ".$data['description']."</p>

										 ";
												}
									$req1 = "SELECT * FROM bureau WHERE id IN (SELECT idEspace FROM reservationsBureau WHERE idClient ='$idClient') ";
									mysqli_set_charset($conn, 'utf8');
									$res1 = mysqli_query($conn, $req1);
									while ($data = mysqli_fetch_assoc($res1)) {

													echo "
															</br>
															<p><img src='img/picto/bureau.png' width='10%'/>	Bureau occupé : ".$data['nom']."</p>
															<p>	<img src='img/picto/lieu.png' width='10%'/> Lieux : ".$data['milieu']."
																 à ".$data['lieu']."</p>
															<p>	<img src='img/picto/prix.png' width='10%'/>prix :".$data['prix']."</p>";
												}
										// $req2 = "SELECT nom  FROM services WHERE id IN (SELECT idService FROM lienSRB WHERE idResaBureau IN (SELECT id FROM reservationsBureau WHERE idClient='$idClient')) ";
										// mysqli_set_charset($conn, 'utf8');
										// $res2 = mysqli_query($conn, $req2);
										// while ($data = mysqli_fetch_assoc($res2)) {
										//
										// 				echo "
										// 							Services inclus : ".$data['nom']." </br>";
										// 			}
										// $req3 = "SELECT quantite FROM lienSRB WHERE idResaBureau IN (SELECT id FROM reservationsBureau WHERE idClient='$idClient') ";
										// mysqli_set_charset($conn, 'utf8');
										// $res3 = mysqli_query($conn, $req3);
										// while ($data = mysqli_fetch_assoc($res3)) {
										//
										// 				echo "
										// 						Quantité : ".$data['quantite']."
										// 				</div>";
										// 			}
										 ?>
								</div>
							</div>
							<!-- /.box-body -->
							<div class="box box-solid">
								<div class="box-header with-border">
									<h3 class="box-title">Vos réservations</h3>
								</div>
								<!-- /.box-header -->
								<div class="box-body">
									<?php
									$conn = connexion();
									// $req1 = "SELECT * FROM salleReunions WHERE id IN (SELECT idEspace FROM reservationsReunion WHERE idClient ='$idClient') ";
									// mysqli_set_charset($conn, 'utf8');
									// $res1 = mysqli_query($conn, $req1);
									// while ($data = mysqli_fetch_assoc($res1)) {
									//
									// 				echo "
									// 						</br>	Salle de réunion :".$data['nom']."</br>
									// 							Dans le :".$data['milieu']."</br>
									// 							à :".$data['lieu']."</br>
									// 							prix :".$data['prix']."</br>
									// 							nombre de personne max : ".$data['nbPersonnes']."</br>
									// 							";
									// 			}
									// $req2 = "SELECT nom  FROM services WHERE id IN (SELECT idService FROM lienSRR WHERE idResaReunion IN (SELECT id FROM reservationsReunion WHERE idClient='$idClient')) ";
									// mysqli_set_charset($conn, 'utf8');
									// $res2 = mysqli_query($conn, $req2);
									// while ($data = mysqli_fetch_assoc($res2)) {
									//
									// 				echo "
									// 							Services inclus : ".$data['nom']." </br>";
									// 			}
									// $req3 = "SELECT quantite FROM lienSRR WHERE idResaReunion IN (SELECT id FROM reservationsReunion WHERE idClient='$idClient') ";
									// mysqli_set_charset($conn, 'utf8');
									// $res3 = mysqli_query($conn, $req3);
									// while ($data = mysqli_fetch_assoc($res3)) {
									//
									// 				echo "
									// 						Quantité : ".$data['quantite']."
									// 				</div>";
									// 			}
									$req = "SELECT * FROM reservationsReunion WHERE idClient='$idClient'";
									mysqli_set_charset($conn, 'utf8');
									$res = mysqli_query($conn, $req);
									while ($data = mysqli_fetch_assoc($res)) {
													$id=$data['id'];

													$req2 = "SELECT * FROM salleReunions WHERE id IN (SELECT idEspace FROM reservationsReunion WHERE id ='$id') ";
													mysqli_set_charset($conn, 'utf8');
													$res2 = mysqli_query($conn, $req2);
													while ($data2 = mysqli_fetch_assoc($res2)) {
														echo "Salle : ".$data2['nom']."
													</br>	";
													}

													echo "Réservée le  :".$data['dateResa']." de : ".$data['HeureDebut']."h	à :".$data['HeureFin']."h</br>";

													$req1 = "SELECT id, nom  FROM services WHERE id IN (SELECT idService FROM lienSRR WHERE idResaReunion='$id') ";
													mysqli_set_charset($conn, 'utf8');
													$res1 = mysqli_query($conn, $req1);
													while ($data1 = mysqli_fetch_assoc($res1)) {
														echo "Service :".$data1['nom']."</br>";
														$idService=$data1['id'];
													$req3 = "SELECT quantite FROM lienSRR WHERE idResaReunion='$id' AND idService='$idService' ";
													mysqli_set_charset($conn, 'utf8');
													$res3 = mysqli_query($conn, $req3);
													while ($data3 = mysqli_fetch_assoc($res3)) {
																	echo "
																		Quantité : ".$data3['quantite']."
																	</br>";
																}
																echo "</br>";
													}
													echo "</br>";
									}




												mysqli_close($conn);
										?>



									<a href="resaSalle.php">Réserver une salle</a>
								</br><a href="resaBureau.php">Réserver un autre bureau</a>

								</div>
							</div>
					</div>
						<!-- /.box -->
				</div>
					<!-- /.col -->
			</div>
				<!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.8
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-yellow"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                <p>New phone +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                <p>nora@example.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-file-code-o bg-green"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                <p>Execution time 5 seconds</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Resume
                <span class="label label-success pull-right">97%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 97%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Laravel Integration
                <span class="label label-warning pull-right">50%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Back End Framework
                <span class="label label-primary pull-right">68%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Other sets of options are available
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Allow the user to show his name in blog posts
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Chat Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Delete chat history
              <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
</body>
</html>
