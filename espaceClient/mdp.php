<?php
	session_start();
	include_once("fonctions.php");
	if (isset($_SESSION['User'])){
      $mail = $_SESSION['User'];
  }
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Woolink</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/square/blue.css">
  <link rel="stylesheet" href="calendar.css">
  <link rel="stylesheet" href="style.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
  <?php

$conn = connexion();
mysqli_set_charset($conn, 'utf8');
if (isset($_POST["mdp1"]) && isset($_POST["mdp2"]) ){
  $mdp1 =  $_POST["mdp1"];
  $mdp2 =  $_POST["mdp2"];
    if ($mdp1 == $mdp2){
    $sql="UPDATE compte SET mdp='$mdp1' WHERE mail='$mail'";
    $result = mysqli_query($conn, $sql);
    header("Status: 301 Moved Permanently", false, 301);
    header("Location: http://localhost:7800/espaceClient/profil.php");
    exit();

  }else{
    echo "les deux mots de passe ne sont pas identiques";
  }
}

 mysqli_close($conn);

  		?>
<div class="login-box">
  <div class="login-logo">
    <img src="entete.png" width="100%" />
    <!-- <a href="../../index2.html"><b>Admin</b>LTE</a> -->

  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
<h2 class="centrer">Définissez un mot de passe :</h2>
<?php
// echo $mail;
?>
    <form action="mdp.php" method="post">
      Entrez un mot de passe :
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="mdp1" required>
      </div>
      Entrez à nouveau un mot de passe :
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="mdp2" required>
      </div>

      <div class="row">
        <div class="bouton" >
          <button type="submit" class="btn btn-primary btn-block btn-flat">Continuer</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <!-- <a href="#">I forgot my password</a><br>
    <a href="register.html" class="text-center">Register a new membership</a> -->

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="plugins/iCheck/icheck.min.js"></script>
<!-- <script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script> -->
<script type="text/javascript" src="mCalandar.js"></script>
</body>
</html>
