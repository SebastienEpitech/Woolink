<?php
require_once "html2php/src/Html2Pdf.php";
$user = array(
    "id" => 1,
    "siret" => "152 356 785",
    "firstname" => "John",
    "lastname" => "Doe",
    "email" => "john.doe@gmail.com",
    "portable" => "06.25.35.45.35",
    "address" => "26 Avenue du Bourg\n75000 Paris"
);
$client = array(
    "id" => 1,
    "firstname" => "Luc",
    "lastname" => "Kennedy",
    "mail" => "luc.kennedy@gmail.com",
    "portable" => "06.32.23.15.58",
    "address" => "5 Avenue du Boulevard Maréchal Juin\n14000 Caen",
    "infos" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium eos tempora, magni delectus porro cum labore eligendi."
);
$project = array(
    "id" => 1,
    "name" => "Création d'un Portfolio",
    "status" => 1,
    "infos" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium eos tempora, magni delectus porro cum labore eligendi.",
    "created" => 1,
    "paid" => false,
    "client_id" => 1,
    "user_id" => 1
);
$tasks[] = array(
    "id" => 1,
    "ref" => "96ER1",
    "description" => "Veille technologique",
    "price" => 200,
    "quantity" => 1,
    "project_id" => 1
);
$tasks[] = array(
    "id" => 2,
    "ref" => "152DE",
    "description" => "Création et intégration d'un thème pour WordPress",
    "price" => 500,
    "quantity" => 1,
    "project_id" => 1
);
$tasks[] = array(
    "id" => 3,
    "ref" => "25365",
    "description" => "Développement d'un plugin WordPress sur mesure pour le client",
    "price" => 1000,
    "quantity" => 1,
    "project_id" => 1
);
?>
<?php

require_once "lib/html2pdf.php";
// Initialisation des données
ob_start();
$total = 0;  $total_tva = 0;
?>
// Insertion du CSS
<page backtop="10mm" backleft="10mm" backright="10mm" backbottom="10mm" footer="page;">
<page_footer>
<hr />
<p>Fait a Paris, le <?php echo date("d/m/y"); ?></p>
<p>Signature du particulier, suivie de la mension manuscrite "bon pour accord".</p>
<p>&amp;nbsp;</p>
</page_footer>
<table style="vertical-align: top;">
<tr>
<td class="75p">
<strong><?php echo $user['firstname']." ".$user['lastname']; ?></strong><br />
<?php echo nl2br($user['address']); ?><br />
<strong>SIRET:</strong> <?php echo $user['siret']; ?><br />
<?php echo $user['email']; ?>
</td>
<td class="25p">
<strong><?php echo $client['firstname']." ".$client['lastname']; ?></strong><br />
<?php echo nl2br($client['address']); ?><br />
</td>
</tr>
</table>
<table style="margin-top: 50px;">
<tr>
<td class="50p"><h2>Devis n°14</h2></td>
<td class="50p" style="text-align: right;">Emis le <?php echo date("d/m/y"); ?></td>
</tr>
<tr>
<td style="padding-top: 15px;" colspan="2"><strong>Objectif:</strong> <?php echo $project['name']; ?></td>
</tr>
</table>
<table style="margin-top: 30px;" class="border">
<thead>
<tr>
<th class="60p">Description</th>
<th class="10p">Quantité</th>
<th class="15p">Prix Unitaire</th>
<th class="15p">Montant</th>
</tr>
</thead>
<tbody>
<?php foreach ($tasks as $task): ?>
<tr>
<td><?php echo $task['description']; ?></td>
<td><?php echo $task['quantity']; ?></td>
<td><?php echo $task['price']; ?> &amp;euro;</td>
<td><?php
$price_tva = $task['price']*1.2;
echo $price_tva;
?>
&amp;euro;</td>
<?php
$total += $task['price'];
$total_tva += $price_tva;
?>
</tr>
<?php endforeach ?>
<tr>
<td class="space"></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td colspan="2" class="no-border"></td>
<td style="text-align: center;" rowspan="3"><strong>Total:</strong></td>
<td>HT : <?php echo $total; ?> &amp;euro;</td>
</tr>
<tr>
<td colspan="2" class="no-border"></td>
<td>TVA : <?php echo ($total_tva - $total); ?> &amp;euro;</td>
</tr>
<tr>
<td colspan="2" class="no-border"></td>
<td>TTC : <?php echo $total_tva; ?> &amp;euro;</td>
</tr>
</tbody>
</table>
</page>
<?php
$content = ob_get_clean();
try {
$pdf = new HTML2PDF("p","A4","fr");
$pdf->pdf->SetAuthor('DOE John');
$pdf->pdf->SetTitle('Devis 14');
$pdf->pdf->SetSubject('Création d\'un Portfolio');
$pdf->pdf->SetKeywords('HTML2PDF, Devis, PHP');
$pdf->writeHTML($content);
$pdf->Output('Devis.pdf');
} catch (HTML2PDF_exception $e) {
die($e);
}
?>
 ?>
