var myNav = document.getElementById('barre');
window.onscroll = function () {
    "use strict";
    if (document.body.scrollTop >= 80 ) {
        myNav.classList.add("nav-colored");
        myNav.classList.remove("nav-transparent");
    }
    else {
        myNav.classList.add("nav-transparent");
        myNav.classList.remove("nav-colored");
    }
};
