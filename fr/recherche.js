function alimente()
{
    if (document.getElementById('select1').value == "Coworking")
    {
        document.getElementById('select2').style.display = "inline";
        document.getElementById('select2').options.length = 0;
        document.getElementById('select2').options[document.getElementById('select2').options.length]=new Option('Gare de Lyon 12eme','Gare de Lyon 12eme');
        document.getElementById('select2').options[document.getElementById('select2').options.length]=new Option('Champs Elysées 8eme','Champs Elysées 8eme');
        document.getElementById('select2').options[document.getElementById('select2').options.length]=new Option('Paris Suchet 16eme','Paris Suchet 16eme');
        document.getElementById('select2').options[document.getElementById('select2').options.length]=new Option('Porte d Orléans','Porte d Orléans');
        document.getElementById('select2').options[document.getElementById('select2').options.length]=new Option('Noisy-Le-Grand','Noisy-Le-Grand');
        document.getElementById('select2').options[document.getElementById('select2').options.length]=new Option('Saint Mandé','Saint Mandé');
        document.getElementById('select2').options[document.getElementById('select2').options.length]=new Option('Bourse 2eme','Bourse 2eme');
        
    }
    else if (document.getElementById('select1').value == "Coliving")
    {
        document.getElementById('select2').style.display = "inline";
        document.getElementById('select2').options.length = 0;
        document.getElementById('select2').options[document.getElementById('select2').options.length]=new Option('Paris 16eme','Paris 16eme');
    }

}


function charger(){
  var selection = $("#select1").val(); //récupère le concept sélectionné
  var opdata = $('#select2').val(); //récupère le lieu selectionné
  // alert (selection);
  // alert (opdata);
  switch (selection) {
    case "Coworking":
      switch (opdata) {
        case "Gare de Lyon 12eme":
            var url="we_lyon.html";
            break;
        case "Champs Elysées 8eme":
            var url="we_champs.html";
            break;
        case "Paris Suchet 16eme":
            var url="we_suchet.html";
            break;
        case "Porte d Orléans":
            var url="we_orleans.html";
            break;
        case "Noisy-Le-Grand":
            var url="we_noisy.html";
            break;
        case "Saint Mandé":
            var url="we_mande.html";
            break;
        case "Bourse 2eme":
            var url="we_bourse.html";
            break;
        }
        break;
    case "Coliving":
      switch (opdata) {
        case "Paris 16eme":
            var url="l_16.html";
            break;
        }
      break;
    default:
        alert("vous n'avez pas sélectionné d'offres");
        break;
  }

  $("#form-submit").attr('action',url);
}
